#!/usr/bin/python3
# menu.py
import psycopg2
from fonction import *

HOST = "tuxa.sme.utc"
USER = "nf18p002"
PASSWORD = "sqFP5ODb59Oh"
DATABASE = "dbnf18p002"

# Connexion à la base de donnée
conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
cur = conn.cursor()


choix = 0
while (choix != 3):
    print("Bienvenue dans la base de donnée du Louvre.\nQui êtes vous ?\n-Administrateur (1)\n-Employé(2)\n-Quitter(3)")
    choix = int(input("Choix : "))
    if choix == 1:
        # Admin
        print("Mode Administrateur.")
        while choix != 6:
            print(
                "Vous pouvez :\n- Afficher les tables (1)\n- Voir une table (2)\n- Insérer une valeur (3)\n- Supprimer une valeur (4)\n- Supprimer une table (5)\n- Quitter (6)")
            choix = int(input("Choix : "))
            if choix == 1:
                afficherTables(cur)
            elif choix == 2:
                voirTable(conn)
            elif choix == 3:
                insererValeur(cur,conn)
            elif choix == 4:
                supprimerValeur(cur,conn)
                print(4)
            elif choix == 5:
                supprimerTable(cur,conn)
            elif choix == 6:
                print("Deconnexion du mode administrateur.")
            else:
                print("Mauvaise saisie.")
    elif choix == 2:
        # Mode simple utilisateur
        print("Mode employé")
        while choix != 6:
            print("Vous pouvez :\n- Visualiser les tables (1)\n- Voir les valeurs d'une tables (2)\n- Les créneaux d'un guide (3)\n- Connaitre le prix d’acquisition moyen par exposition (4)\n- Connaitre le temps moyen d'emprunt des oeuvres du Louvre par d'autres musées (5)\n- Voir les oeuvres en cours de restauration (6)\n- Savoir la capacité des salles d'une expo (7)\n- Quitter (8)")
            choix = int(input("Votre choix : "))
            if choix==1:
                afficherTables(cur)
            elif choix==2:
                nom_table = input("Quelle table voulez voir : ")
                voirTable(conn, nom_table)
            elif choix==3:
                nom_guide = input("Entrer le nom du guide : ")
                prenom_guide = input("Entrer le prenom du guide : ")
                voirCrenauxGuide(conn,nom_guide,prenom_guide)
            elif choix==4:
                prixMoyenExpo(conn)
            elif choix==5:
                tempsMoyen(cur)
            elif choix==6:
                oeuvreRestauratino(cur)
            elif choix==7:
                print("Deconnexion employé")
            else:
                print("Mauvaise saisie.")
    elif choix == 3:
        print("Merci d'avoir utilisé l'application. Au revoir.")
    else:
        print("Mauvaise saisie.")
