import pandas as pd
import matplotlib.pyplot as plt

# ---------------Fonctions Administrateur---------------------

def afficherTables(cur):
    cur.execute("SELECT tablename FROM pg_tables WHERE schemaname='public'")
    tables = cur.fetchone()
    print("\nLa liste des tables :")
    while (tables):
        for nom in tables:
            print("-", nom)
        tables = cur.fetchone()
    print("\n")


def voirTable(conn, nom_table):
    df = pd.read_sql(f"SELECT * FROM {nom_table}", conn)
    plt.table(cellText=df.values, colLabels=df.columns, loc='center')
    plt.axis('off')
    plt.show()


def insererValeur(cur,conn):
    table = input("Dans quelle table voulez vous insérer la valeur : ")
    sql = f"SELECT column_name FROM information_schema.columns WHERE TABLE_NAME = '{table}'"
    cur.execute(sql)
    raw = cur.fetchall()
    print(f"{raw[0][0]} = ",end="")
    v = input()
    if v.isdigit():
        val = v
    else:
        val = "'"+v+"'"
    for row in raw[1:]:
        print(f"{row[0]} = ",end="")
        v = input()
        if v.isdigit() :
            val = f"{val}, {v}"
        else :
            val = f"{val}, '{v}'"
    sql=f"INSERT INTO {table} VALUES ({val});"
    cur.execute(sql)
    conn.commit()
    print("La valeur a bien été insérée")


def supprimerValeur(cur,conn):
    table = input("Dans quelle table voulez vous supprimer une valeur : ")
    sql = f"SELECT pg_attribute.attname FROM pg_index, pg_class, pg_attribute WHERE pg_class.oid = '{table}'::regclass AND indrelid = pg_class.oid AND pg_attribute.attrelid = pg_class.oid AND pg_attribute.attnum = any(pg_index.indkey) AND indisprimary;"
    cur.execute(sql)
    key_label = cur.fetchone()[0]
    valeur = input("Entrer " + key_label + " : ")
    sql = f"DELETE FROM {table} WHERE {key_label} = '{valeur}';"
    print(sql)
    cur.execute(sql)
    conn.commit()


def supprimerTable(cur,conn):
    table = input("Quelle est la table que vous souhaitez supprimer : ")
    sql = f"DROP TABLE IF EXISTS {table} CASCADE;"
    cur.execute(sql)
    conn.commit()

# ---------------Fonctions Employé---------------------

def voirCrenauxGuide(conn,guide_nom,guide_prenom):
    sql = f"SELECT expo,jour,horaire FROM guide,affectationcreneau WHERE nom = '{guide_nom}' AND prenom = '{guide_prenom}' AND id = guide;"
    df = pd.read_sql(sql,conn)
    plt.table(cellText=df.values, colLabels=df.columns, loc='center')
    plt.axis('off')
    plt.show()

def prixMoyenExpo(conn):
    sql = f"SELECT * FROM v_prix_expos;"
    df = pd.read_sql(sql, conn)
    plt.table(cellText=df.values, colLabels=df.columns, loc='center')
    plt.axis('off')
    plt.show()

def tempsMoyen(cur):
    sql = "SELECT AVG(DATE_PART('day', CAST(fin AS TIMESTAMP) - CAST(debut AS TIMESTAMP)) + 1) as duree FROM Echange WHERE pret_louvre = true;"
    cur.execute(sql)
    raw = cur.fetchall()[0]
    print("Le temps actuel moyen des emprunts des oeuvres du Louvre par d'autres musée est ",raw," jours.")

def oeuvreRestauratino(cur) : #affiche les oeuvres en cours de restauration
    sql="SELECT O.titre, A.prenom, A.nom FROM Oeuvre O, Auteur A, Restauration WHERE R.oeuvre = O.id AND A.id = O.auteur AND R.fin IS NULL;"
    cur.execute(sql)
    print("Les oeuvres en cours de restauration sont :")
    raw=cur.fetchone()
    while (raw) :
        print(f"{raw[0]} ({raw[1]} {raw[2]})")
        raw=cur.fetchone()

def capaciteSalle(cur) :
    voirTable(conn,"expoTemporaire")
    expo=input("Pour quelle exposition voulez-vous voir la capacité des salles ? (Nom) :" )
    sql=f"SELECT S.numero, S.capacite FROM Salle S, Panneau P, ExpoTemporaire ET WHERE ET.nom = '{expo} AND S.numero = P.salle AND P.expo = ET.nom ORDER BY S.capacite DESC;"
    cur.execute(sql)
    print(f"Les capacités des salles pour l'exposition {expo} sont : \n")
    raw=cur.fetchone()
    while (raw) :
        print(f"{raw[0]} : {raw[1]} places")
        raw=cur.fetchone()
    

