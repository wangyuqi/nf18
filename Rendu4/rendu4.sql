--
-- #################### DROP TABLE ####################
--
DROP TABLE IF EXISTS AffectationCreneau, AffectationOccasionnelle, OeuvreExpoTemporaire, OeuvreExpoPermanante, Echange, Panneau, Salle, Guide, ExpoTemporaire, ExpoPermanante, OeuvreExt, MuseeExt, OeuvreLouvre, Restauration, Oeuvre, Auteur;


--
-- #################### CREATE TABLE ####################
--
CREATE TABLE Auteur(
	id INT PRIMARY KEY,
	nom VARCHAR(25) NOT NULL,
	prenom VARCHAR(25) NOT NULL,
	naissance DATE NOT NULL CHECK(naissance<CURRENT_DATE),
	mort DATE,
	CHECK(mort>naissance),
	UNIQUE(nom,prenom,naissance)
	);

CREATE TABLE Oeuvre(
	id INT PRIMARY KEY,
	titre VARCHAR(50) NOT NULL,
	date DATE NOT NULL,
	type VARCHAR(12) CHECK(type='sculpture' OR type='photographie' OR type='peinture') NOT NULL,
	dim VARCHAR(11) NOT NULL,
	auteur int REFERENCES Auteur(id) NOT NULL
	);

CREATE TABLE Restauration(
	id INT PRIMARY KEY,
	oeuvre INT REFERENCES Oeuvre(id) NOT NULL,
	debut DATE NOT NULL,
	fin DATE,
	CHECK(fin>debut)
);

CREATE TABLE MuseeExt(
	id INT PRIMARY KEY,
	nom VARCHAR(50) NOT NULL,
	adresse VARCHAR(255) NOT NULL,
	UNIQUE(adresse)
);

CREATE TABLE OeuvreLouvre (
    id INT REFERENCES Oeuvre(id) PRIMARY KEY,
    prix INT NOT NULL CHECK (prix>=0)
);

CREATE TABLE OeuvreExt (
    id INT REFERENCES Oeuvre(id) PRIMARY KEY,
    musee INT REFERENCES MuseeExt(id)  NOT NULL
);

CREATE TABLE ExpoPermanante(
    nom VARCHAR(50) PRIMARY KEY
);

CREATE TABLE ExpoTemporaire (
    nom VARCHAR(50) PRIMARY KEY,
    debut DATE NOT NULL,
    fin DATE NOT NULL,
    CHECK (debut<fin)
);

CREATE TABLE Guide(
    id INT PRIMARY KEY,
    nom VARCHAR(25) NOT NULL,
    prenom VARCHAR(25) NOT NULL,
    adresse VARCHAR(255) NOT NULL,
    embauche DATE NOT NULL CHECK(embauche<=CURRENT_DATE)
);

CREATE TABLE Salle(
    numero INT PRIMARY KEY,
    capacite INT NOT NULL
);

CREATE TABLE Panneau(
    expo VARCHAR(50) REFERENCES ExpoTemporaire(nom),
    numero INT,
    texte VARCHAR(255) NOT NULL,
    salle INT NOT NULL REFERENCES Salle(numero),
    PRIMARY KEY (expo,numero)
);

CREATE TABLE Echange(
    oeuvre INT REFERENCES Oeuvre(id),
    musee INT REFERENCES MuseeExt(id),
    pret_louvre BOOL NOT NULL,
    debut DATE NOT NULL,
    fin DATE NOT NULL,
    PRIMARY KEY (oeuvre, musee),
    CHECK(fin>debut)
);

CREATE TABLE OeuvreExpoPermanante (
    expo VARCHAR(50) REFERENCES ExpoPermanante(nom),
    oeuvre INT REFERENCES OeuvreLouvre(id),
    PRIMARY KEY (expo, oeuvre)
);

CREATE TABLE OeuvreExpoTemporaire (
    expo VARCHAR(50),
    oeuvre INT,
    PRIMARY KEY (expo, oeuvre),
    FOREIGN KEY (expo) REFERENCES ExpoTemporaire(nom),
    FOREIGN KEY (oeuvre) REFERENCES OeuvreExt(id)
);

CREATE TABLE AffectationCreneau (
    guide INT REFERENCES Guide (id),
    expo VARCHAR(50) REFERENCES ExpoPermanante (nom),
    jour VARCHAR(10) CHECK (jour = 'lundi' OR jour = 'mardi' OR jour = 'mercredi' OR jour = 'jeudi' OR jour = 'vendredi' OR jour = 'samedi' OR jour = 'dimanche'),
    horaire TIME NOT NULL CHECK (horaire > '09:00:00' AND horaire < '16:00:01'),
    PRIMARY KEY (guide, expo, jour)
);

CREATE TABLE AffectationOccasionnelle (
    guide INT REFERENCES Guide(id),
    expo VARCHAR(50) REFERENCES ExpoTemporaire(nom),
    debut TIMESTAMP NOT NULL,
    fin TIMESTAMP NOT NULL,
    PRIMARY KEY (guide,expo,debut),
    CHECK (debut < fin)
);


--
-- #################### CREATE VIEW ####################
--

-- Contraintes complexes

CREATE VIEW v_oeuvres_exclusif AS
    SELECT id
    FROM OeuvreLouvre
    INTERSECT
    SELECT id
    FROM OeuvreExt;

CREATE VIEW v_oeuvres_héritage_fille AS
    SELECT id
    FROM OeuvreLouvre
    UNION
    SELECT id
    FROM OeuvreExt;

CREATE VIEW v_exposition AS
    SELECT id
    FROM MuseeExt
    INTERSECT
    SELECT musee
    FROM OeuvreExt;

CREATE VIEW v_heritage_expo AS
    SELECT nom
    FROM ExpoPermanante
    INTERSECT
    SELECT nom
    FROM ExpoTemporaire;


-- Vue pour l'application python

CREATE VIEW v_prix_expos AS
    SELECT OEP.expo, avg(prix) AS MoyennePrixAcquisition
    FROM OeuvreExpoPermanante OEP, OeuvreLouvre O
    WHERE O.id = OEP.oeuvre
    GROUP BY expo;


--
-- #################### INSERT VALUE ####################
--

-- # AUTEURS #
INSERT INTO Auteur VALUES (1, 'De Vinci', 'Léonard', '14-04-1452', '02-05-1519');
INSERT INTO Auteur VALUES (2, 'David', 'Jacques-Louis', '30-08-1748', '29-12-1825');
INSERT INTO Auteur VALUES (3, 'Botticelli', 'Sandro', '01-03-1444', '17-05-1510');
INSERT INTO Auteur VALUES (4, 'Anonyme', 'Nemo', '01-01-1950', '01-01-2023');
INSERT INTO Auteur VALUES (5, 'Mazzola', 'Francesco', '11-01-1503', '24-08-1540');
INSERT INTO Auteur VALUES (6, 'Johannes', 'Vermeer', '31-10-1632', '15-12-1675');
INSERT INTO Auteur VALUES (7, 'Delacroix', 'Eugène', '26-04-1789', '13-08-1863');
INSERT INTO Auteur VALUES (8, 'Monet', 'Claude', '14-11-1840', '05-12-1926');
INSERT INTO Auteur VALUES (9, 'Van-Gogh', 'Vincent', '30-03-1853', '29-07-1890');
INSERT INTO Auteur VALUES (10, 'Munch', 'Edvard', '12-12-1863', '23-01-1944');
INSERT INTO Auteur VALUES (11, 'Picasso', 'Pablo', '25-10-1881', '08-04-1973');
INSERT INTO Auteur VALUES (12, 'Warhol', 'Andy', '06-08-1928', '22-02-1987');
INSERT INTO Auteur VALUES (13, 'Matisse', 'Henri', '31-12-1869', '03-11-1954');
INSERT INTO Auteur VALUES (14, 'Pollock', 'Jackson', '28-01-1912', '11-08-1956');
INSERT INTO Auteur VALUES (15, 'Magritte', 'René', '21-11-1898', '15-08-1967');

-- # OEUVRE #
INSERT INTO Oeuvre VALUES (1, 'La Joconde', '01-01-1504', 'peinture', '77x53', 1);
INSERT INTO Oeuvre VALUES (2, 'Le sacre de Napoléon 1er', '01-01-1805', 'peinture', '27,5x42,5',2);
INSERT INTO Oeuvre VALUES (3, 'Madone du Magnificat', '01-01-1485', 'peinture', '141x141',3);
INSERT INTO Oeuvre VALUES (4, 'figurine de Horus harponneur', '01-01-0001', 'sculpture', '20x2', 4);
INSERT INTO Oeuvre VALUES (5, 'Antea', '01-01-1527', 'peinture', '136x86', 5);
INSERT INTO Oeuvre VALUES (6, 'La nuit étoilée', '01-06-1889', 'peinture', '74x92', 9);
INSERT INTO Oeuvre VALUES (7, 'La Liberté guidant le peuple', '01-01-1830', 'peinture',  '260x325', 7);
INSERT INTO Oeuvre VALUES (8, 'Le Bassin aux nymphéas', '01-01-1919', 'peinture', '100x201', 8);
INSERT INTO Oeuvre VALUES (9, 'The Joy of Life', '01-01-1906', 'peinture', '176x240', 13);
INSERT INTO Oeuvre VALUES (10, 'Le Reve', '01-01-1932', 'peinture', '130x97', 11);
INSERT INTO Oeuvre VALUES (11, 'Guernica', '01-04-1937', 'peinture', '350x780', 11);
INSERT INTO Oeuvre VALUES (12, 'Number 5', '20-04-1948', 'peinture', '35x40', 14);
INSERT INTO Oeuvre VALUES (13, 'Number 1', '21-11-1950', 'peinture', '35x40', 14);
INSERT INTO Oeuvre VALUES (14, 'Golconde', '01-11-1953', 'peinture', '35x40', 15);
INSERT INTO Oeuvre VALUES (15, 'Les Amants', '22-01-1929', 'peinture', '35x40', 15);

-- # RESTAURATION # 
INSERT INTO Restauration VALUES (1, 1, '04-05-2022', '28-07-2022');
INSERT INTO Restauration VALUES (2, 1, '04-05-2000', '28-07-2001');
INSERT INTO Restauration VALUES (3, 2, '01-02-2023', '31-05-2023');
INSERT INTO Restauration VALUES (4, 6, '15-06-2021', '20-10-2021');
INSERT INTO Restauration VALUES (5, 7, '01-01-2022', '15-03-2022');
INSERT INTO Restauration VALUES (6, 8, '01-03-2024', '31-05-2024');
INSERT INTO Restauration VALUES (7, 8, '01-09-2023', '31-12-2023');
INSERT INTO Restauration VALUES (8, 7, '01-07-2022', '31-10-2022');
INSERT INTO Restauration VALUES (9, 9, '01-11-2021', '28-02-2022');
INSERT INTO Restauration VALUES (10, 12, '01-05-2023',NULL);
INSERT INTO Restauration VALUES (11, 14, '05-07-2020',NULL);

-- # MUSEE_EXT #
INSERT INTO MuseeExt VALUES (1, 'Musée du Quai Branly', '37 Quai Branly Paris VII');
INSERT INTO MuseeExt VALUES (2, 'Musée du Vatican', 'Viale Vaticano snc, 00120 Vatican City');
INSERT INTO MuseeExt VALUES (3, 'British Museums', 'Great Russell Street London WC1B 3DG');
INSERT INTO MuseeExt VALUES (4, 'Royal Museums of Fine Arts of Belgium', 'Rue de la Régence, Place Royale 1, Bruxelles, BE 1000');
INSERT INTO MuseeExt VALUES (5, 'Le musée de Capodimonte', 'Via Miano, 2 Naples');
INSERT INTO MuseeExt VALUES (6, 'Musée d Orsay', '1 Rue de la Légion d Honneur, 75007 Paris');

-- # OEUVRE_LOUVRE #
INSERT INTO OeuvreLouvre VALUES (1, 20000000);
INSERT INTO OeuvreLouvre VALUES (2, 5000000);
INSERT INTO OeuvreLouvre VALUES (6, 4000000);
INSERT INTO OeuvreLouvre VALUES (7, 900000);
INSERT INTO OeuvreLouvre VALUES (8, 7560000);
INSERT INTO OeuvreLouvre VALUES (9, 5430000);
INSERT INTO OeuvreLouvre VALUES (12, 905500);
INSERT INTO OeuvreLouvre VALUES (13, 709000);
INSERT INTO OeuvreLouvre VALUES (14, 809000);
INSERT INTO OeuvreLouvre VALUES (15, 449000);

-- # OEUVRE_EXT #
INSERT INTO OeuvreExt VALUES (3,5);
INSERT INTO OeuvreExt VALUES (5,5);
INSERT INTO OeuvreExt VALUES (4,4);
INSERT INTO OeuvreExt VALUES (10,4);
INSERT INTO OeuvreExt VALUES (11,4);

-- # EXPO_PERMANANTE #
INSERT INTO ExpoPermanante VALUES ('La Renaissance');
INSERT INTO ExpoPermanante VALUES ('Les impressionnistes');
INSERT INTO ExpoPermanante VALUES ('La modernité');
INSERT INTO ExpoPermanante VALUES ('Artistes Français');

-- # EXPO_TEMPORAIRE #
INSERT INTO ExpoTemporaire VALUES ('Naples à Paris', '07-06-2023', '23-09-2025');
INSERT INTO ExpoTemporaire VALUES ('Egypte antique', '07-03-2023', '23-12-2023');
INSERT INTO ExpoTemporaire VALUES ('Picasso une vie', '31-08-2023', '23-10-2023');

-- # GUIDE #
INSERT INTO Guide VALUES (1, 'Capet', 'Hugues', '7 avenue des mimosas Paris XIV', '04-06-2021');
INSERT INTO Guide VALUES (2, 'De la Roche', 'Victor-Emmanuel', '32 rue du four Paris XII', '04-06-2019');
INSERT INTO Guide VALUES (3, 'Alberti', 'Louis-Amédée', '18 rue de la motte Paris XX', '28-02-2020');
INSERT INTO Guide VALUES (4, 'Lancaster', 'Marie-Laeticia', '54 chemin du puit Paris XI', '04-12-2010');
INSERT INTO Guide VALUES (5, 'D Aquitaine', 'Aliénor', '3 Avenue de la Victoire Paris VI', '16-04-1999');
INSERT INTO Guide VALUES (6, 'De Provence', 'Marguerite', '13 rue des beauxmonts Paris II', '04-11-2021');

-- # SALLE #
INSERT INTO Salle VALUES (1,100);
INSERT INTO Salle VALUES (2,50);
INSERT INTO Salle VALUES (3,80);
INSERT INTO Salle VALUES (4,120);
INSERT INTO Salle VALUES (5,20);
INSERT INTO Salle VALUES (6,45);

-- # PANNEAU #
INSERT INTO Panneau VALUES ('Naples à Paris', 1, 'Un ensemble d oeuvres des plus grand peintres italiens', 1);
INSERT INTO Panneau VALUES ('Naples à Paris', 2, 'Portraits' , 2);
INSERT INTO Panneau VALUES ('Egypte antique', 1, 'Horus' , 3);
INSERT INTO Panneau VALUES ('Egypte antique', 2, 'Pyramide' , 4);
INSERT INTO Panneau VALUES ('Picasso une vie', 1, 'Phase sereine', 5);
INSERT INTO Panneau VALUES ('Picasso une vie', 2, 'Cri politique', 6);

-- # ECHANGE #
INSERT INTO Echange VALUES (3, 5, FALSE, '01-02-2021', '01-01-2026');
INSERT INTO Echange VALUES (5, 5, FALSE, '01-02-2021', '01-01-2026');
INSERT INTO Echange VALUES (4, 4, FALSE, '01-01-2023', '30-12-2023');
INSERT INTO Echange VALUES (10, 5, FALSE, '01-01-2023', '30-12-2023');
INSERT INTO Echange VALUES (11, 5, FALSE, '01-01-2023', '30-12-2023');
INSERT INTO Echange VALUES (2, 1, TRUE, '01-02-2017', '22-09-2017');
INSERT INTO Echange VALUES (2, 3, TRUE, '11-06-2021', '01-09-2021');
INSERT INTO Echange VALUES (6, 4, TRUE, '01-01-2023', '30-12-2023');
INSERT INTO Echange VALUES (12, 5, TRUE, '01-01-2023', '30-12-2023');
INSERT INTO Echange VALUES (15, 1, TRUE, '01-02-2017', '22-09-2017');
INSERT INTO Echange VALUES (15, 6, TRUE, '01-02-2015', '05-11-2016');

-- # OEUVRE_EXPO_PERMANANTE #
INSERT INTO OeuvreExpoPermanante VALUES ('La Renaissance',1);
INSERT INTO OeuvreExpoPermanante VALUES ('Les impressionnistes',8);
INSERT INTO OeuvreExpoPermanante VALUES ('La modernité',12);
INSERT INTO OeuvreExpoPermanante VALUES ('La modernité',13);
INSERT INTO OeuvreExpoPermanante VALUES ('La modernité',14);
INSERT INTO OeuvreExpoPermanante VALUES ('La modernité',15);
INSERT INTO OeuvreExpoPermanante VALUES ('Artistes Français', 7);
INSERT INTO OeuvreExpoPermanante VALUES ('Artistes Français', 8);
INSERT INTO OeuvreExpoPermanante VALUES ('Artistes Français', 9);
INSERT INTO OeuvreExpoPermanante VALUES ('Artistes Français', 6);

-- # OEUVRE_EXPO_TEMPORAIRE #
INSERT INTO OeuvreExpoTemporaire VALUES ('Naples à Paris',5);
INSERT INTO OeuvreExpoTemporaire VALUES ('Naples à Paris',3);
INSERT INTO OeuvreExpoTemporaire VALUES ('Egypte antique',4);
INSERT INTO OeuvreExpoTemporaire VALUES ('Picasso une vie', 10);
INSERT INTO OeuvreExpoTemporaire VALUES ('Picasso une vie', 11);

-- # AFFECTATION_CRENEAU #
INSERT INTO AffectationCreneau VALUES (1, 'Les impressionnistes', 'mardi', '09:30:00');
INSERT INTO AffectationCreneau VALUES (1, 'Les impressionnistes', 'jeudi', '09:30:00');
INSERT INTO AffectationCreneau VALUES (2, 'Les impressionnistes', 'samedi', '09:30:00');
INSERT INTO AffectationCreneau VALUES (5, 'Artistes Français', 'mardi', '09:30:00');
INSERT INTO AffectationCreneau VALUES (6, 'Artistes Français', 'vendredi', '15:00:00');
INSERT INTO AffectationCreneau VALUES (3, 'La modernité', 'mardi', '10:30:00');
INSERT INTO AffectationCreneau VALUES (3, 'La modernité', 'vendredi', '14:15:00');
INSERT INTO AffectationCreneau VALUES (4, 'La Renaissance', 'lundi', '10:30:00');
INSERT INTO AffectationCreneau VALUES (4, 'La Renaissance', 'mercredi', '15:30:00');

-- # AFFECTATION_OCCASIONNELLE #
INSERT INTO AffectationOccasionnelle VALUES (1, 'Naples à Paris', TO_TIMESTAMP('2019-03-12 08:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2019-03-14 16:00:00', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO AffectationOccasionnelle VALUES (1, 'Egypte antique', TO_TIMESTAMP('2019-03-15 08:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2019-03-17 16:00:00', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO AffectationOccasionnelle VALUES (1, 'Picasso une vie', TO_TIMESTAMP('2019-03-18 08:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2019-03-20 16:00:00', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO AffectationOccasionnelle VALUES (2, 'Naples à Paris', TO_TIMESTAMP('2019-03-12 08:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2019-03-14 16:00:00', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO AffectationOccasionnelle VALUES (2, 'Egypte antique', TO_TIMESTAMP('2019-03-15 08:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2019-03-17 16:00:00', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO AffectationOccasionnelle VALUES (2, 'Picasso une vie', TO_TIMESTAMP('2019-03-18 08:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2019-03-20 16:00:00', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO AffectationOccasionnelle VALUES (3, 'Naples à Paris', TO_TIMESTAMP('2019-03-12 08:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2019-03-14 16:00:00', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO AffectationOccasionnelle VALUES (3, 'Egypte antique', TO_TIMESTAMP('2019-03-15 08:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2019-03-17 16:00:00', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO AffectationOccasionnelle VALUES (3, 'Picasso une vie', TO_TIMESTAMP('2019-03-18 08:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_TIMESTAMP('2019-03-20 16:00:00', 'YYYY-MM-DD HH24:MI:SS'));


--
-- #################### REQUETES ####################
-- Voici quelques requêtes pour tester notre base de données, d'autres requêtes seront ajoutée dans l'application python, comme des requêtes pour savoir le temps moyen que passe les oeuvres empruntées au Louvre ou d'autres statistiques
--
--
-- 1. Toutes les expositions

SELECT E.nom 
FROM ExpoPermanante E
UNION
SELECT E.nom 
FROM ExpoTemporaire E;

-- 2. Les noms, prénom et dates de naissance des auteurs qui ont fait des oeuvres entre 1400 et 1500

SELECT A.nom, A.prenom, A.naissance
FROM Auteur A, Oeuvre O
WHERE A.id = O.auteur AND O.date BETWEEN '01-01-1400' AND '01-01-1500';

-- 3. Le nom, le prénom et l’id des guides qui ont un créneau pour une expo du tableau 'La Joconde'

SELECT DISTINCT G.id, G.nom, G.prenom
FROM Guide G, AffectationCreneau A, ExpoPermanante E, OeuvreExpoPermanante OE, Oeuvre O
WHERE O.titre = 'La Joconde' AND OE.oeuvre = O.id AND OE.expo = E.nom AND A.expo = E.nom AND A.guide = G.id;

-- 4. Les guides qui ont un créneau en commun

SELECT DISTINCT cg1.guide, cg2.guide
FROM AffectationCreneau cg1
INNER JOIN AffectationCreneau cg2
ON cg1.jour = cg2.jour
AND cg1.horaire = cg2.horaire
WHERE cg1.guide != cg2.guide;

-- 5. Le titre des oeuvres du Louvre empruntées au moins deux fois

SELECT O.titre, COUNT(*) AS Nombre_Emprunts
FROM Oeuvre O, Echange E
WHERE pret_louvre AND E.oeuvre = O.id 
GROUP BY O.titre
HAVING COUNT(*) >= 2;

-- 6. Le titre et les noms et prénoms de l’auteurs des oeuvres en cours de restaurations 

SELECT O.titre, A.nom, A.prenom
FROM Oeuvre O, Auteur A, Restauration R
WHERE R.oeuvre = O.id AND A.id = O.auteur AND R.fin IS NULL;

-- 7. Le nom et l’adresse des musées qui ont emprunté des oeuvres sans que le Louvre ne leur en emprunte

SELECT M.nom, M.adresse
FROM MuseeExt M LEFT OUTER JOIN Echange E ON E.musee = M.id AND NOT pret_louvre 
WHERE E.debut IS NULL
INTERSECT
SELECT DISTINCT M.nom, M.adresse
FROM MuseeExt M, Echange E
WHERE E.musee = M.id AND E.pret_louvre = TRUE;

-- 8. La capacité des salles de l’expo 'Naples à Paris' par ordre décroissant

SELECT S.numero, S.capacite
FROM Salle S, Panneau P, ExpoTemporaire ET
WHERE ET.nom = 'Naples à Paris' AND S.numero = P.salle AND P.expo = ET.nom
ORDER BY S.capacite DESC;

-- 9. Les oeuvres qui étaient en restauration au moment où la guide 'De Provence Marguerite' a été embauché 

SELECT O.titre
FROM Restauration R, Oeuvre O, Guide G
WHERE R.oeuvre = O.id AND G.embauche > R.debut AND G.embauche < R.fin AND G.nom = 'De Provence' AND G.prenom = 'Marguerite';

-- 10. Prix d’acquisition moyen par exposition

SELECT OEP.expo, avg(prix) AS MoyennePrixAcquisition
FROM OeuvreExpoPermanante OEP, OeuvreLouvre O
WHERE O.id = OEP.oeuvre
GROUP BY expo;

-- 11. Les oeuvres les plus restaurées (les oeuvres restaurées par ordre decroissant)

SELECT O.titre,count(*) AS Nombre_Restaurations
FROM Restauration R, Oeuvre O
WHERE R.oeuvre = O.id
group by O.titre
order by count(*) DESC;

-- 12. Le nombre d’oeuvres par auteur

SELECT COUNT(O.id), A.nom, A.prenom 
FROM Oeuvre O, Auteur A
WHERE O.auteur = A.id
GROUP BY A.id;





